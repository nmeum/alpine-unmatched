# Alpine Linux on the HiFive Unmatched

Scripts to create a SD card for booting [Alpine Linux][alpine web] Edge on the [HiFive Unmatched][unmatched web].

## Status

As per the HiFive Unmatched [manual][unmatched manual], the HiFive
Unmatched can boot from either SPI flash or the SD card. The script in
this repository is intended to boot from the SD card, it would be nice
to also allow installing U-Boot to the SPI flash by updating Alpine's
`update-u-boot` script from `main/u-boot` accordingly.

The SD card image creation script is also a bit rough on the edges. For
one thing, Alpine does currently not have official "Generic RISC-V"
release image thus the rootfs is created manually as part of the script.
As soon as the riscv64 images are available the script can be heavily
simplified.

## Pre-built Images

If you don't have the capabilities to build your own image, you can
download an unofficial pre-built image from [my webspace][nmeum unmatched].
These images only have to be flashed to the SD card using `dd(1)`, see
the next section for more information.

## Usage

The script is intended to be run on Alpine Linux Edge and thus uses
busybox utilities and requires a working [`apk(8)`][apk-tools web]
installation. Furthermore, a few packages need to be installed, e.g.
using the following command:

	# apk add sgdisk e2fsprogs qemu-riscv64 qemu-openrc

The qemu packages are necessary since Alpine doesn't have official
pre-built images riscv64 yet. For this reason, the script creates a root
filesystem on the fly for which qemu is required to emulate a riscv64
chroot on the host. This requires `qemu-riscv64` to be registered with
[`binfmt_misc`][linux binfmt], e.g. using:

	# rc-service qemu-binfmt start

Afterwards, it should be possible to simply run:

	# ./build_image.sh

This should create a `unmatched.img` file in the current subdirectory.
This file can then be flashed to the HiFive Unmatched SD card using:

	# dd if=unmatched.img of=/dev/mmcblk0 bs=1M oflag=direct

Afterwards, put the SD card into the HiFive Unmatched, start it as usual
and a autologin prompt should appear on the SiFive UART which can be
used to configure the system and (hopefully) soon install U-Boot to the
SPI flash and a rootfs to a NVME storage using [alpine-conf][alpine-conf web].

## License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

[alpine web]: https://alpinelinux.org
[unmatched web]: https://www.sifive.com/boards/hifive-unmatched
[unmatched manual]: https://sifive.cdn.prismic.io/sifive/de1491e5-077c-461d-9605-e8a0ce57337d_fu740-c000-manual-v1p3.pdf
[aports !24350]: https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/24350
[apk-tools web]: https://gitlab.alpinelinux.org/alpine/apk-tools
[alpine-conf web]: https://gitlab.alpinelinux.org/alpine/alpine-conf
[linux binfmt]: https://www.kernel.org/doc/html/latest/admin-guide/binfmt-misc.html
[nmeum unmatched]: https://dev.alpinelinux.org/~nmeum/hifive-unmatched/
