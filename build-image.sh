#!/bin/sh

set -euo pipefail

IMAGE="unmatched-$(date +%Y%m%d).img"
MNTPNT="$(mktemp -d /tmp/unmatched.XXXXXX)"
LOOP_DEVICE="$(losetup -f)"

if [ "$(id -u)" -ne 0 ]; then
	echo "Script needs to be run as root" 1>&2
	exit 1
fi

##
# Create image file.
##

# Create image file and partition it according to the SiFive FU740-C000 Manual.
# According to Section 9 (Boot Process):
#
#  1. ZSBL is in ROM and loads U-Boot SPL from a partition with
#     type 5B193300-FC78-40CD-8002-E86C45580B47.
#  2. U-Boot SPL is then loaded, loads the next U-Boot stage from
#     a partition with type 2E54B353-1271-4842-806F-E436D6AF69851.
#  3. Last U-Boot stages looks for a partition with type
#     0FC63DAF-8483-4772-8E79-3D69D8477DE4 (Linux Filesystem) and
#     searches for extlinux.conf. For more information, consult
#     the files `./include/configs/sifive-unmatched.h` as well as
#     `./doc/README.distro` in the U-Boot source tree.
#
# As such, we create three GPT partations with the mentioned types
# in the following. No type is specified for partition three as
# Linux Filesystem is the default partition type value.
#
# Total image size and size of last partition are adjustable.

cleanup() {
	set +e
	sync
	umount "$MNTPNT"
	losetup -d "${LOOP_DEVICE}"
	rm -rf "$MNTPNT"
}

trap cleanup EXIT INT

truncate -s 1536M "$IMAGE"

sgdisk -g --clear --set-alignment=1 \
	--new=1:34:+1M:         --change-name=1:'u-boot-spl'    --typecode=1:5b193300-fc78-40cd-8002-e86c45580b47 \
	--new=2:2082:+4M:       --change-name=2:'opensbi-uboot' --typecode=2:2e54b353-1271-4842-806f-e436d6af6985 \
	--new=3:16384:+500M:    --change-name=3:'root'          --attributes=3:set:2 \
	"${IMAGE}"

losetup -P "${LOOP_DEVICE}" "${IMAGE}"

for partition in 1 2 3; do
	if [ ! -b "${LOOP_DEVICE}p${partition}" ]; then
		echo "Partition '${LOOP_DEVICE}p${partition}' does not exist" 1>&2
		exit 1
	fi
done

##
# Alpine rootfs.
##

mkfs.ext4 "${LOOP_DEVICE}p3"
mount -text4 "${LOOP_DEVICE}p3" "$MNTPNT"

# TODO: Alpine doesn't generic release images for RISC-V yet.
# For this reason, we are forced to create our own rootfs for now.
#
# This requires installing qemu-riscv64 and qemu-openrc as well as
# starting the qemu-binfmt OpenRC services. Otherwise, the post-install
# scripts cannot be executed.
apk --quiet --root "$MNTPNT" --arch riscv64 --initdb --update-cache --allow-untrusted \
	-X http://dl-cdn.alpinelinux.org/alpine/edge/main \
	-X http://dl-cdn.alpinelinux.org/alpine/edge/community \
	add alpine-base linux-edge linux-firmware-none ca-certificates u-boot-unmatched

##
# Write bootloaders from newroot to image partitions
##

dd if="$MNTPNT"/usr/share/u-boot/sifive_unmatched/u-boot-spl.bin \
	of="$LOOP_DEVICE"p1 bs=4k
dd if="$MNTPNT"/usr/share/u-boot/sifive_unmatched/u-boot.itb \
	of="$LOOP_DEVICE"p2 bs=4k

# Activate various OpenRC services by default
ln -s /etc/init.d/bootmisc "$MNTPNT"/etc/runlevels/boot/
ln -s /etc/init.d/hostname "$MNTPNT"/etc/runlevels/boot
ln -s /etc/init.d/modules "$MNTPNT"/etc/runlevels/boot
ln -s /etc/init.d/sysctl "$MNTPNT"/etc/runlevels/boot
ln -s /etc/init.d/urandom "$MNTPNT"/etc/runlevels/boot
ln -s /etc/init.d/devfs "$MNTPNT"/etc/runlevels/sysinit
ln -s /etc/init.d/hwdrivers "$MNTPNT"/etc/runlevels/sysinit
ln -s /etc/init.d/mdev "$MNTPNT"/etc/runlevels/sysinit
ln -s /etc/init.d/modules "$MNTPNT"/etc/runlevels/sysinit
ln -s /etc/init.d/mount-ro "$MNTPNT"/etc/runlevels/shutdown
ln -s /etc/init.d/killprocs "$MNTPNT"/etc/runlevels/shutdown

# Automatically login on SiFive Serial.
# XXX: picocom doesn't like the password prompt for some reason.
printf '\n# Auto login on SiFive serial\n%s\n' \
	'ttySIF0::respawn:/bin/login -f root' \
	>> "$MNTPNT"/etc/inittab

mkdir -p "$MNTPNT"/extlinux
cat <<EOF > "$MNTPNT"/extlinux/extlinux.conf
menu title HiFive Unmatched
timeout 50
default alpine

label alpine
	menu label Alpine Linux
	kernel /boot/vmlinuz-edge
	fdt /boot/dtbs-edge/sifive/hifive-unmatched-a00.dtb
	append earlyprintk earlycon=sbi rw root=/dev/mmcblk0p3 rootfstype=ext4 rootwait console=ttySIF0,115200
EOF
